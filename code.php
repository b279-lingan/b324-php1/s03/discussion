<?php 

$buildingObj = (object)[
	'name' => 'Caswynn Building',
	'floors' => '8',
	'address' => (object)[
		'barangay' => 'Sacred Heart',
		'city' => 'Quezon City',
		'country' => 'Philippines'
	]
];

// Create an object using a Class
class Building {

	public $name;
    public $floors;
    public $address;
    public $zipCode;

	// A constructor is used during the creation of an object
	public function __construct($name, $floors, $address, $zipCode){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
		$this->zipCode = $zipCode;
	}

	// methods of function

public function printName(){
		return "The name of the building is $this->name.";
	}
	
public function checkFloors(){
		return "$this->floors";
	}

};

$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City', '0000');
$secondBuilding = new Building('Trial Building', 100, 'Sa lugar kung saan di mo alam.', '0002');

// Inheritance
class Condominium extends Building{
	public $rooms;

	public function __construct($name, $floors, $address, $zipCode, $rooms){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
		$this->zipCode = $zipCode;
		$this->rooms = $rooms;
	}

	public function printName(){
		return "The name of the condominium is $this->name.";
	}

	public function checkFloors(){
		return "$this->floors with $this->rooms.";
	}

	public function checkZipCode(){
		return "The zip code is $this->zipCode";
	}
}

$condominium = new Condominium('Trial Condominium', 50, 'Manila City, Manila', '0001', 500);

// Abstaction
abstract class Drink{
	public $name;

	public function __construct($name){
		$this->name = $name;
	}

	public abstract function getDrinkName();
}

class Coffee extends Drink{
	public function getDrinkName(){
		return "The name of the coffee is $this->name.";
	}
}

$kopiko = new Coffee('Kopiko');












?>